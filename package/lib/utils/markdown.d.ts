import { LoaderContext } from "webpack";
export interface CodeHandlerParams {
    /** 高亮解析后的代码块 html */
    codeHTML: string;
    /** 基于那种语言进行高亮解析 */
    highlightLang: string;
    /** 高亮解析前的代码块 markdown */
    codeSource: string;
    /** 解析 markdown 代码块定义的语言 */
    sourceLang: string;
}
export interface CodeHandler {
    (params: CodeHandlerParams): string;
}
export type ContainerOpts = MarkdownResolverOptions["containerOptions"] & {};
export interface ContainerRender {
    (tokens: any[], idx: number, simpleMarkdownIt: {
        [k: string]: any;
    }, containerOpts: ContainerOpts): string;
}
export interface MarkdownResolverOptions {
    /** 预设模式 */
    preset?: "default" | "commonmark" | "zero";
    /** 包裹整个 html 的 div.class */
    contentClass?: string;
    /** prismjs无法解析语言的映射：{ 读取的语言: 目标语言 } */
    languageMap?: {
        [k: string]: string;
    };
    /** markdown-it 配置：是否在源码中启用 HTML 标签 */
    html?: boolean;
    /** markdown-it 配置：是否使用 "/" 来闭合单标签 */
    xhtmlOut?: boolean;
    /** markdown-it 配置：转换段落里的 '\n' 到 <br> */
    breaks?: boolean;
    /** markdown-it 配置：<pre /> 代码块的 CSS 类名前缀 */
    langPrefix?: string;
    /** markdown-it 配置：将类似 URL 的文本自动转换为链接 */
    linkify?: boolean;
    /** markdown-it 配置：启用一些语言中立的替换 + 引号美化 */
    typographer?: boolean;
    /** markdown-it 配置：双 + 单引号替换对，当 typographer 启用时 */
    quotes?: string;
    /** 使用 markdown 实例作为入参，提供更好的自定义 */
    handler?: (markdownIt: any, simpleMarkdownIt: any, loaderContext?: MarkdownLoaderContent) => void;
    codeHandler?: CodeHandler;
    /** 是否使用 markdown-it-anchor 插件 */
    useAnchor?: boolean;
    /** markdown-it-anchor 的配置 */
    anchorOptions?: object;
    /** 是否使用 markdown-it-toc-done-right 插件 */
    useTOC?: boolean;
    /** markdown-it-toc-done-right 的配置 */
    TOCOptions?: {
        [k: string]: any;
    };
    /** 是否使用 markdown-it-container 插件  */
    useContainer?: boolean;
    /** markdown-it-container 配置 */
    containerOptions?: {
        containerClass?: string;
        titleClass?: string;
        bodyClass?: string;
        validate?: (params: string) => string;
        render?: (tokens: any[], idx: number) => string;
        marker?: string;
        renderMap?: {
            [k: string]: ContainerRender;
        };
    };
}
export interface MarkdownResolver {
    /** markdownIt 实例 */
    markdownIt: any;
    /** 用于处理的 markdown 文本的解析器 */
    resolver: (source: string) => {
        html: string;
        TOCCode: string;
    };
    /** 更新 markdownIt */
    update: (opts: MarkdownResolverOptions) => void;
}
export type MarkdownLoaderOptions = MarkdownResolverOptions & {
    /** prismjs 加载的代码块语言类型："all" | [langs]。*/
    languages: "all" | string[];
};
export type MarkdownLoaderContent = LoaderContext<MarkdownLoaderOptions>;
export declare function useMarkdownResolver(options?: MarkdownResolverOptions, loaderContext?: MarkdownLoaderContent): MarkdownResolver;
