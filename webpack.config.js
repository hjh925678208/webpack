import HtmlWebpackPlugin from "html-webpack-plugin"
import TerserPlugin from "terser-webpack-plugin"
import { MarkdownLoader } from "@imhjh/webpack"

export default (env) => {
  return {
    mode: "development",
    entry: "./src/index.js",
    plugins: [new HtmlWebpackPlugin({ template: "./src/index.html" })],
    module: {
      rules: [
        {
          test: /\.md$/,
          use: [
            {
              loader: MarkdownLoader,
              options: {
                languages: "all",
                languageMap: { vue: "html", default: "js" }
              }
            }
          ]
        },
        {
          test: /\.tsx?$/,
          use: "ts-loader",
          exclude: /node_modules/
        },
        {
          test: /\.(css|scss|sass)$/i,
          oneOf: [{ use: ["style-loader", "css-loader", "sass-loader"] }]
        }
      ]
    },
    resolve: {
      extensions: [".tsx", ".ts", ".js", "mjs"]
    },
    optimization: {
      minimize: true,
      minimizer: [
        new TerserPlugin({
          extractComments: false
        })
      ]
    },
    stats: {
      errorDetails: true
    }
  }
}
