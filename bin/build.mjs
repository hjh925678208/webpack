import { resolve, dirname } from "path"
import { fileURLToPath } from "url"
import { rmSync, readFileSync, writeFileSync } from "fs"
import { execSync } from "child_process"

// esm 模式读取 __dirname
const __filename = fileURLToPath(import.meta.url)
const __dirname = dirname(__filename)

const libPath = resolve(__dirname, "../package/lib")

// 删除lib文件夹
try {
  rmSync(libPath, { recursive: true })
} catch (err) {}

// 更新 readme.md
const sourceReadme = resolve(__dirname, "../README.md")
const targetReadme = resolve(__dirname, "../package/README.md")
try {
  const data = readFileSync(sourceReadme)
  writeFileSync(targetReadme, data)
} catch (err) {}

// 打包
execSync("tsc", { stdio: "inherit" })
