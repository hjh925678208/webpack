import { resolve, dirname } from "path"
import { readFileSync, writeFile } from "fs"
import { fileURLToPath } from "url"
import { execSync } from "child_process"
import inquirer from "inquirer"

// esm 模式读取 __dirname
const __filename = fileURLToPath(import.meta.url)
const __dirname = dirname(__filename)

// 读取 package
const packagePath = resolve(__dirname, "../package/package.json")
const file = readFileSync(packagePath)
const packageData = JSON.parse(file.toString())

// 解析版本号
const [major, minor, patch] = packageData.version.split(".")
const Major = parseInt(major) + 1
const Minor = parseInt(minor) + 1
const Patch = parseInt(patch) + 1

// 命令行提问选择版本号
const answers = inquirer.prompt([
  {
    type: "list",
    name: "version",
    message: "请选择要发布的版本号",
    choices: [`主要版本 ${Major}.0.0`, `次要版本 ${major}.${Minor}.0`, `补丁版本 ${major}.${minor}.${Patch}`, `当前版本 ${major}.${minor}.${patch}`]
  }
])
answers.then((answers) => {
  const { version } = answers
  const [, res] = version.split(" ")
  writeVersion(res)
})

// 修改 package 中的版本号
function writeVersion(version) {
  packageData.version = version
  const packageStr = JSON.stringify(packageData, undefined, 2)
  writeFile(packagePath, `${packageStr}\n`, function (err) {
    if (err) throw err
    else release()
  })
}

// 执行发布命令
function release() {
  execSync("cd ./package && npm publish --access=public --registry=https://registry.npmjs.com/", {
    stdio: "inherit" // 将子进程输出到主进程窗口中
  })
}
