import TestMarkdown from "./test.md"
import { useMarkdownResolver } from "@imhjh/webpack/utils"

const { resolver } = useMarkdownResolver({
  languageMap: { default: "js" },
  useAnchor: true,
  useTOC: true,
  useContainer: true
})

const { html } = resolver(TestMarkdown.source)

const app = document.getElementById("app")
app.innerHTML = `${html}`

// import { useMarkdownResolver } from "@imhjh/webpack/utils"
// const { resolver } = useMarkdownResolver()
// const app = document.getElementById("app")
// app.innerHTML = resolver("# 标题")
