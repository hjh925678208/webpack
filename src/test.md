# 我展示的是一级标题

## 我展示的是二级标题

### 我展示的是三级标题

#### 我展示的是四级标题

##### 我展示的五级标题

###### 我展示的是六级标题

---

# 字体

_斜体文本_
**粗体文本**
**_粗斜体文本_**

# 列表

- 第一项
  - 1.1
  - 1.2
- 第二项

  1. 第一项
  2. 第二项
  3. 第三项

- 第三项

1. 第一项
2. 第二项
3. 第三项

4. 第一项：
   - 第一项嵌套的第一个元素
   - 第一项嵌套的第二个元素
5. 第二项：
   - 第二项嵌套的第一个元素
   - 第二项嵌套的第二个元素

# 区块

> 学的不仅是技术更是梦想

> 最外层
>
> > 第一层嵌套
> >
> > > 第二层嵌套

> 区块中使用列表
>
> 1. 第一项
> 2. 第二项
>
> - 第一项
> - 第二项
> - 第三项

- 第一项
  > 菜鸟教程
  > 学的不仅是技术更是梦想
- 第二项

# 代码块

`hello`

```javascript
/* $(document).ready(function () {
  alert("hello")
  console.log(1)
}) */

const hi = 5
hi = 10
// 🛑 Uncaught TypeError: Assignment to constant variable.
console.log(hi)
// -> 5
```

```bash
> npm install
```

# 表格

| 表头   |   表头 |  表头  |
| :----- | -----: | :----: |
| 单元格 | 单元格 | 单元格 |
| 单元格 | 单元格 | 单元格 |

# 高级语法

<div>
<kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>Del</kbd>
</div>

# 链接

[页面内锚点](#区块)

[站内链接 - 首页](/)

[外部链接 - @imhjh/common](https://www.npmjs.com/package/@imhjh/common)

# 图片

![logo 图标](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMsAAABKCAYAAAAVF6BOAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAHWSURBVHhe7dzRTuJQFEBRZv7/n2dshKQhbdlUJLR3rcT4oFFOOZsWJVyCf9cPGNqf6+c195E8+n7GdduV0+7I1mBrZxPBcG+IB9W1ocpll2jWleO35IjH9DceVD/y+P29fp6rN3TvQJzH1g5MXzvVjtzH8uxwghlXve9PsyPzWPYOJZjxPHufn2JHbrH8dBjBjGPvfX34HZlisehUQz+oLj3B38NfxigOvSdTLNMAPxlCKOMYek/mZ5Y9wwhlPMPuyf1l2DNDCWVcQ+7J0nOWMpxQGG5P1p7gi4Fia09Ot0NrsUymYZcGFhJza3tyOlux3MwPhFBYc/o9sfy/Y+8/39wf3z7y+JUzC/BFLBCJBaL5Nd47rhMP/UK6L3XWT53zFdf0w+6JMwtEYoFILBCJBSKxQCQWiMQCkVggEgtEYoFILBCJBSKxQHSUVx2X3/HoZ7/rZ0zecSyXvOr2b3nHbB95/JxZIBILRGKBSCwQiQUisUAkFojEApFYIBILRGKBSCwQiQUisUAkFojEApFYIBILRGKBSCwQiQUisUAkFojEAtEr3nSNc9n7BnePHH7XnFkgEgtEYoFILBCJBSKxQCQWiMQCkVggEgtEYoFILBCJBZLL5T89xi+T/DKUQAAAAABJRU5ErkJggg== "logo")

# 自定义容器

::: default default 容器

```bash
> npm install
```

:::

::: tip tip 容器

```bash
> npm install
```

:::

::: warning warning 容器

```bash
> npm install
```

:::

::: danger danger 容器

测试

```bash
> npm install
```

:::

::: component `这是自定义的组件`

details

:::
